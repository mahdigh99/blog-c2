<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Models\User;

class UserController extends Controller
{
    /**
     * Show the profile for a given user.
     *
     */
    public function show()
    {
        // $users = DB::select('select * from users');
        // $users = DB::table('users')->get();
        $users = User::all();

        // $users = User::where('name', 'mahdi')->get();

        return $users;
    }

    public function register(Request $request)
    {
        // $input = $request->all();
        // DB::insert('insert into users (name, email, password) values (?, ?, ?)'
                    // , [$input['name'],  $input['email'], $input['password'] ]);

        // $user = new User;
        // $user->name = $request->name;
        // $user->email = $request->email;
        // $user->password = $request->password;
        // $user->save();

        if (!$request->name || !$request->email) {
            # code...
            return response('Please fill required field.', 400);
        }

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => $request->password
        ]);

        return "user registerd!";
    }

    public function update(Request $request)
    {
        // $input = $request->all();
        // $affected = DB::update(
        //     'update users set nickname = ? where id = ?',
        //     [$input['nickname'], $input['id']]
        // );

        $user = User::find($request->id);
        $user->name = $request->name;
        $user->save();

        return $user;
    }


    public function delete(Request $request)
    {
        // $input = $request->all();
        // $deleted = DB::delete('delete from users where id = ?' , [$input['id']]);
        
        $user = User::find($request->id);
        $user->delete();
        return $user;
    }






}
