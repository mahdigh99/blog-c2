<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index_admin()
    {
        $posts = Post::all();
        return view('admin.posts_index', ["posts" => $posts]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $posts = Post::orderByDesc('id')->get();
        return view('blog', ["posts" => $posts]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.posts_create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        try {
            // $featured_picture_url = "";
            // if ($request->hasFile('featured_picture')) {
                //  Let's do everything here
                // if ($request->file('featured_picture')->isValid()) {
                //     //
                //     $validated = $request->validate([
                //         'featured_picture' => 'mimes:jpeg,png|max:4048',
                //     ]);
                //     $extension = $request->featured_picture->extension();
                //     $name = $request->featured_picture->getClientOriginalName();
                //     $request->featured_picture->storeAs('/public/uploads', $name.".".$extension);
                //     $url = Storage::url("/uploads".$name.".".$extension);
                //     $featured_picture_url = $url;
                    // $file = File::create([
                    // 'name' => $name,
                    //     'url' => $url,
                    // ]);
                // }
            // }else{
            //     return 'hi';
            // }
            $path = null;
            $date = Carbon::now()->format('Y-M');
            if ($request->hasFile('featured_picture')) {
                $path = $request->file('featured_picture')->store('featured_pictures/' . $date );
            }
            $user = Auth::user();
            $post = Post::create([
                'user_id' => $user->id ,
                'title' => $request->title,
                'content' => $request->content,
                'featured_picture' => $path
            ]);
            
            return 'پست با موفقیت ایجاد شد.';
        } catch (\Throwable $th) {
            return $th;
            return 'مشکلی به وجود آمده است!';
        }
        

        // $post = new Post; 
        // $post->user_id = 1;
        // $post->title = $request->title;
        // $post->content = $request->content;
        // $post->featured_picture = " ";
        // $post->save();

        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post, $id)
    {
        $post = Post::find($id);
        return view('admin.posts_edit' , $post);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Post $post , $id)
    {
        // if (!$request->title || !$request->content ) {
        //     # code...
        //     return "فیلدهارو پر کنید.";
        // }

        try {
            //code...
            $post = Post::find($id);
            $post->title = $request->title;
            $post->content = $request->content;
            $post->save();
        } catch (\Throwable $th) {
            // throw $th;
            return "مشکلی به وجود آمده!";
        }


        return "پست با موفقیت ویرایش شد";
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            //code...
            $post = Post::find($id);
            $post->delete();
        } catch (\Throwable $th) {
            //throw $th;

            // return response('Not found!', 404);
            return "مشکلی به وجود آمده";
        }

        return "با موفقیت حذف شد";
    }


    public function get_pic($id)
    {
        $post = Post::find($id);
        return Storage::download($post->featured_picture);
    }

    public function test()
    {
        
        // return $date;
    }
}
